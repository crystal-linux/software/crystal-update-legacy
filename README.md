# crystal-update
Update indicator for Crystal Linux and GNOME Shell

## To use with other terminals
Default update command tries to run Gnome Console.
You need to either install gnome-console or edit update command in advanced options of the extension to run with other terminals. Few examples -

gnome-terminal - `gnome-terminal -- /bin/sh -c "ame upg ; echo Done - Press enter to exit; read _"`

BlackBox installed through flatpak - `flatpak run com.raggesilver.BlackBox -c '/bin/sh -c "ame upg ; echo Done - Press enter to exit; read _" '`

## Features
- Uses pacman's «checkupdates» by default and thus does not need root access
- Optional update count display on panel
- Optional notification on new updates (defaults to off)
- Launcher for your favorite update command
- Comes in English, French, Czech, German, Spanish, Brazilian Portuguese, Italian, Polish, Romanian, Arabic, Slovak, Chinese, Serbian, Swedish, Norwegian Bokmal, Russian, Persian, Turkish, Esperanto, Finnish, Dutch, Ukrainian, Korean, Occitan, hungarian languages. (Thanks translators !)

## Requirements
If you use the default "checkupdates" way you might need to install "pacman-contrib". It should be already installed and configured on Crystal Linux.

## On Crystal Linux

On Crystal Linux it should already be ready to go provided by the `crystal-update` package.

## Install for non Crystal Linux, Arch based distributions
Crystal Update Indicator is forked from Archlinux Updates indicator -
- It's on extensions.gnome.org :
https://extensions.gnome.org/extension/1010/archlinux-updates-indicator/

- Install from AUR
Thanks to michiwend you can install it from Arch User Repository : gnome-shell-extension-arch-update
https://aur.archlinux.org/packages/gnome-shell-extension-arch-update/

- For Manual install, simply download as zip and unzip contents in ~/.local/share/gnome-shell/extensions/arch-update@RaphaelRochet

## Credits

Upstream project - 
https://github.com/RaphaelRochet/arch-update
